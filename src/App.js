import React, { Component } from "react";

import Main from "./components/main";
import Nav from "./components/navBar";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productQuantity: 0,
      addedToCart: false,
      emptyCart: false,
      filledCart: false,
      cartClickCount: 0
    };
  }
  handleShowCart = () => {
    let cartClickCount = this.state.cartClickCount;
    cartClickCount += 1;
    this.setState({ cartClickCount });
    if (cartClickCount > 1) {
      this.setState({
        emptyCart: false,
        filledCart: false,
        cartClickCount: 0
      });
      return;
    } else if (this.state.addedToCart) {
      this.setState({ filledCart: true, emptyCart: false });
    } else {
      this.setState({
        emptyCart: true,
        filledCart: false
      });
    }
  };
  handleIncrement = () => {
    let { productQuantity } = this.state;
    productQuantity += 1;
    this.setState({ productQuantity });
  };
  handleDecrement = () => {
    let { productQuantity } = this.state;
    if (productQuantity == 0) {
      return;
    } else {
      productQuantity -= 1;
      this.setState({ productQuantity });
    }
  };
  handleAddToCart = () => {
    if (this.state.productQuantity > 0) {
      this.setState({
        addedToCart: true,
        emptyCart: false
      });
    }
  };

  handleDelete = () => {
    this.setState({
      emptyCart: true,
      filledCart: false,
      productQuantity: 0,
      addedToCart: false
    });
  };

  render() {
    return (
      <div className="App">
        <Nav
          quantity={this.state.productQuantity}
          addedToCart={this.state.addedToCart}
          emptyCart={this.state.emptyCart}
          filledCart={this.state.filledCart}
          onDelete={this.handleDelete}
          showCart={this.handleShowCart}
        />
        <Main
          quantity={this.state.productQuantity}
          onincrease={this.handleIncrement}
          onDecrement={this.handleDecrement}
          onAddToCart={this.handleAddToCart}
          addedToCart={this.state.addedToCart}
        />
      </div>
    );
  }
}

export default App;
