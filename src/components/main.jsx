import React, { Component, Fragment } from "react";

import product1 from "../images/image-product-1.jpg";
import product2 from "../images/image-product-2.jpg";
import product3 from "../images/image-product-3.jpg";
import product4 from "../images/image-product-4.jpg";
import thumbnail1 from "../images/image-product-1-thumbnail.jpg";
import thumbnail2 from "../images/image-product-2-thumbnail.jpg";
import thumbnail3 from "../images/image-product-3-thumbnail.jpg";
import thumbnail4 from "../images/image-product-4-thumbnail.jpg";
import plus from "../images/icon-plus.svg";
import minus from "../images/icon-minus.svg";
import cart2 from "../images/icon-cart.svg";
import "./styles.css";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: [product1, product2, product3, product4],
      thumbnails: [thumbnail1, thumbnail2, thumbnail3, thumbnail4],
      index: 0
    };
  }
  handleThumbnail = index => {
    this.setState({ index });
  };

  render() {
    return (
      <Fragment>
        <div className="container">
          {this.props.addedToCart ? (
            <span className="show-cart-quantity">{this.props.quantity}</span>
          ) : null}

          <div className="product">
            <div>
              <img
                className="main-product"
                src={this.state.product[this.state.index]}
                alt=""
              />
            </div>
            <div>
              {this.state.thumbnails.map((thumbnail, index) => {
                return (
                  <img
                    key={index}
                    onClick={() => this.handleThumbnail(index)}
                    className="thumbnail"
                    src={thumbnail}
                    alt=""
                  />
                );
              })}
            </div>
          </div>
          <div className="content">
            <p>SNEAKER COMPANY</p>
            <p>Fall Limited Edition Sneakers</p>
            <p>
              These low-profile sneakers are your perfect casual wear companion.
              Featuring a durable rubber outer sole, they’ll withstand
              everything the weather can offer.
            </p>
            <div>
              <div>
                <p>$125.00</p>
                <p>50%</p>
              </div>
              <p>
                <del>$250</del>
              </p>
            </div>
            <div>
              <div>
                <button onClick={this.props.onDecrement}>
                  <img src={minus} alt="minus-sign" />
                </button>
                <p>{this.props.quantity}</p>
                <button onClick={this.props.onincrease}>
                  <img src={plus} alt="plus-sign" />
                </button>
              </div>
              <button onClick={this.props.onAddToCart}>
                <img src={cart2} alt="" />
                <p>Add to cart</p>
              </button>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Main;
